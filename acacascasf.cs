﻿using CommonUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportCore;
using TestStack.White.Configuration;

namespace PLM.Tests
{
    /// <summary>
    ///  This class contains test cases related to project management area.
    /// </summary>
    [TestClass]
    public class SearchProject : TestBase
    {
        private CommonGUI Main = new CommonGUI();

        [TestMethod]
        public void SearchProjectTest()
        {
            Reporter reporter = new Reporter("SearchProjectTest");
            PLMGUI Srch = new PLMGUI(reporter);
            CoreAppXmlConfiguration.Instance.BusyTimeout = 20000;
            Main.ClickOnMenuItem("Publisher");
         
            Srch.ClickDefineSearchCriteriaItem();
            Srch.EnterProductNumber("Sub-group");
            Srch.SetFindScriteria();
            Srch.SetValueFromModalWindow("002");
            Srch.AcceptSearchCriteria();
			asdonjao	
			poasoi
            Srch.FindProject();
            Srch.SelectFindedProject("9000");
            Srch.AcceptSearchCriteria();
            Srch.CreateProject();
            Srch.ClickNewPrint();
            Srch.SetNewProjectNumber("11121218");
            Srch.SaveProject();
			supermetoda();
            Srch.AcceptDialogButton();
        }
    }
}
