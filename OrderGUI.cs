﻿using CommonUtilities;
using ReportCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace Order
{
    public class OrderGUI : Basic
    {
        private String AppTitle = WindowTitle.GetPreparedTitleFromPosition(0);
        private Window Orderwnd;

        private String Search = "31";
        private String CustomerField = "229376";
        private String LeveringAdresseCustomer = "331776";
        private String Formular = "1867776";

        public OrderGUI(Reporter report)
            : base(report)
        {
            report = reporter;
        }
        public OrderGUI()
        {

        }
        /// <summary>
        /// Finds 'Order' link button and clicks on it. Checks if "Order - SPS DK (1)" main window shows up.
        /// </summary>
        public void ClickOnOrder()
        {
            Thread.Sleep(MediumWait);
            Window wnd = WaitFor(MainApp);
            Logger.Log("TITLE: ");
            if (wnd == null)
            {
                Logger.Log("Schilling main window not found");
            }
            SearchCriteria sc1 = SearchCriteria.ByText("Order").AndIndex(1);
            try
            {
                Object o = WaitFor(sc1,wnd);
                Logger.Log(o.ToString());
                UIItem el = (UIItem)o;
                el.Click();
                Notify("Click on Order link button", true);
                wnd.WaitWhileBusy();
                Thread.Sleep(7000);
            }
            catch (Exception e)
            {
                Logger.Log("ClickOnOrder throw " + e.ToString(), Exc);
                Notify("Cannot click on Order link button", false);
                throw;
            }
            Thread.Sleep(5000);
            Orderwnd = WaitFor(AppTitle);
            if (Orderwnd == null)
            {
                Logger.Log("Order - SPS DK (1) WINDOW IS NULL");
            }
            else
            {
                Logger.Log("Order - SPS DK (1) WINDOW WAS FOUND");
                Logger.Log(wnd.ToString());
            }
        }
        /// <summary>
        /// Finds 'Order' textbox in "Order - SPS DK (1)" dialog. Clicks on it and presses RETURN to bring up order type selection dialog.
        /// </summary>
       
        /// <summary>
        /// Dissmisses order type selection dialog by pressing F12 key.
        /// </summary>
        public void DismissSelectOrdertype()
        {
            Thread.Sleep(MediumWait);
            Press(F12);
        }
        /// <summary>
        /// Finds customer text box in invoice section. Fills the field.
        /// </summary>
        public void EnterCustomerName(String customerName)
        {
            Window wnd = WaitFor(AppTitle);
            Thread.Sleep(MediumWait);
            try
            {
                SetText(customerName);
                Notify("Set customer name : " + customerName, true);
            }
            catch (Exception e)
            {
                Logger.Log("EnterCustomerName throw " + e.ToString(), Exc);
                Notify("Cannot set customer name : " + customerName, false);
                throw;
            }
            Press(Enter);
            Logger.Log("In EnterCustomerName set value: " + customerName);
        }
        /// <summary>
        /// Fills product number in "Linjer" data grid.
        /// </summary>
        public void EnterProductNumber(String number)
        {
            Thread.Sleep(MediumWait);
            Window wnd = WaitFor(AppTitle);
            SearchCriteria search = SearchCriteria.ByControlType(ControlType.Table).AndAutomationId(Search);
            SearchCriteria s1 = SearchCriteria.ByClassName("SchGridView").AndAutomationId(Search);
            SearchCriteria s3 = SearchCriteria.ByClassName("Edit").AndIndex(0);
            Panel tb1;
            //This was tested and worked at least in laptop and second monitor
            int deldax = 100;
            int deltay = 20;
            int x;
            int y;
            Point point1 = new Point(0, 0);
            UIItem nb;
            try
            {
                //!!!!!!!!!!!!!!BIG PROBLEM HERE
                //THE PROBLEM IS THAT WE HAVE TO CLICK ON THE PRODUCT NUMBER ROW TO MAKE THE ELEMENTS OF PANEL TB1 ACCESSIBLE TO AUTOMATION
                tb1 = (Panel)wnd.Get(s1);
                Logger.Log("PROPERTIES OF THE CONTROL");
                Logger.Log(tb1.ToString());
                x = (int)tb1.Bounds.Left + deldax;
                y = (int)tb1.Bounds.Top + deltay;
                point1.X = x;
                point1.Y = y;
                tb1.Mouse.Location = point1;
                tb1.Mouse.Click();
                Thread.Sleep(1000);
                var o = tb1.GetElement(s3);
                nb = WaitFor(s3, wnd);
                var o1 = tb1.Items.Count;
                Thread.Sleep(2000);
                if (o == null)
                {
                    Logger.Log("o IS NULL");
                }
                else
                {
                    Logger.Log(o.ToString());
                }
                Logger.Log(o1.ToString());
                Logger.Log(nb.ToString());
                SetText(number);
                Press(Tab);
            }
            catch (Exception e)
            {
                Logger.Log("EnterProductNumber throw " + e.ToString(), Exc);
                throw;
            }
            Logger.Log("In EnterProductNumber set value :" + number);
        }
		
		
		supermetora coakscd(){
			
			asdadasdasdasdasd
		}
        /// <summary>
        /// Fills Ordered value in "Lines" data grid.
        /// </summary>
        public void FillOrderedValue(String amount)
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            try
            {
                SetText(qweqweqweqweqwe);
                Notify("Fills Ordered value in 'Lines' data grid: " + amount, true);
            }
            catch (Exception e)
            {
                Logger.Log("FillBestiltValue throw " + e.ToString(), Exc);
                Notify("Cannot fills Ordered value in 'Lines' data grid: " + amount, false);
                throw;
            }
            Press(Enter);
            Logger.Log("In FillOrderedValue set value : " + amount);
        }
        /// <summary>
        /// Fills Delivered value in "Lines" data grid.
        /// </summary>
        public void FillDeliveredValue(String amount)
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);

            try
            {
                SetText(amount);
                Notify("Set delivered value in 'Lines' data grid : " + amount, true);
            }
            catch (Exception e)
            {
                Logger.Log("FillDeliveredValue throws " + e.ToString(), Exc);
                Notify("Cannot set delivered value in 'Lines' data grid : " + amount, false);
                throw;
            }
            Press(Enter);
            Logger.Log("In FillDeliveredValue set value : " + amount);
        }
        /// <summary>
        /// Fills unit price value in "Lines" data grid.
        /// </summary>
        public void FillUnitPriceValue(String amount)
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            try
            {
                SetText(amount);
                Notify("Set unit price value in 'Lines' data grid: " + amount, true);
            }
            catch (Exception e)
            {
                Logger.Log("FillUnitPriceValue throws " + e.ToString(), Exc);
                Notify("Cannot set unit price value in 'Lines' data grid: " + amount, false);
                throw;
            }
            Press(Enter);
            Logger.Log("In FillUnitPricaValue set value : " + amount);
        }
        /// <summary>
        /// Checks if MessageBox (or any modal dialog) shows up. If it does, dismiss it clicking on default button
        /// </summary>
        public void DismissMessageBoxIfExists()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            List<Window> dialogs = wnd2.ModalWindows();
            if (dialogs.Count > 0)
            {
                Press(Enter);
            }
        }
        /// <summary>
        /// Fills discount value in "Linnes" data grid.
        /// </summary>
        public void FillDiscountValue(String amount)
        {
            Thread.Sleep(MediumWait);
            try
            {
                SetText(amount);
                Notify("Set discount value in 'Lines' data grid: " + amount, true);
            }
            catch (Exception e)
            {
                Logger.Log("FillDiscountValue throws " + e.ToString(), Exc);
                Notify("Cannot set discount value in 'Lines' data grid: " + amount, false);
                throw;
            }
            Press(Enter);
            Logger.Log("In FillDiscountValue set value : " + amount);
        }
        /// <summary>
        /// Adds new line in Lines grid.
        /// </summary>
        public void AddNewLineInLingerGrid()
        {
            Press(F12);
        }

        /// <summary>
        /// Fills AutoSchipping value.
        /// </summary>
        public void FillAutoSchippingValue(String amount)
        {
            Thread.Sleep(MediumWait);
            try
            {
                SetText(amount);
            }
            catch (Exception e)
            {
                Logger.Log("FillAutoSchippingValue throws " + e.ToString(), Exc);
                throw;
            }
            Press(Enter);
            Logger.Log("In FillAutoSchippingValue set value : " + amount);
        }
        /// <summary>
        /// Finds customer text box in invoice section and clicks on it. Then presses return.
        /// </summary>
        public void ClickOnCustomerField()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria value = SearchCriteria.ByAutomationId("CustomerField").AndIndex(0);
            UIItem item;
            try
            {
                item = WaitFor(value,wnd2);
            }
            catch (Exception e)
            {
                Logger.Log("ClickOnCustomerField throws " + e.ToString(), Exc);
                throw;
            }
            item.Click();
            Thread.Sleep(SmallWait);
            Press(Enter); ;
        }
        /// <summary>
        /// Finds customer text box in residence address section and clicks on it. Then presses return.
        /// </summary>
        public void LeveringAdresseCustomerClick()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria value = SearchCriteria.ByAutomationId(LeveringAdresseCustomer).AndIndex(0);
            UIItem adress;
            try
            {
                adress = WaitFor(value, wnd2);
                Notify("Finds customer text box in residence address section and clicks on it", true);
            }
            catch (Exception e)
            {
                Logger.Log("LeveringAdresseCustomerClick throws " + e.ToString(), Exc);
                Notify("Finds customer text box in residence address section and clicks on it", true);
                throw;
            }
            adress.Click();
            Thread.Sleep(SmallWait);
            Press(Enter);
        }
        /// <summary>
        /// First clicks on customer field. Then waits until upper menu rebuilds. Lastly clicks on Print button.
        /// </summary>
        public void PrintClick()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria udskriv = SearchCriteria.ByText("Print").AndControlType(ControlType.Button);
            UIItem cust;

            try
            {
                cust = WaitFor(udskriv, wnd2);
            }
            catch (Exception e)
            {
                Logger.Log("PrintClick throws " + e.ToString(), Exc);
                throw;
            }
            cust.Click();
            Thread.Sleep(MediumWait);
        }
        /// <summary>
        /// Scrolls to the top of the screen using F12 key.
        /// </summary>
        public void ScrollToTheTop()
        {
            Press(F12);
        }
        public void ClickPostPrintMethod()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria post = SearchCriteria.ByText("Post").AndControlType(ControlType.Button);
            UIItem cust;
            try
            {
                cust = WaitFor(post, wnd2);
                Notify("Click on post print method", true);
            }
            catch (Exception e)
            {
                Logger.Log("ClickPostPrintMethod throws " + e.ToString(), Exc);
                Notify("Cannot click on post print method", false);
                throw;
            }
            cust.Click();
            Thread.Sleep(MediumWait);
            Press(Enter);
            Thread.Sleep(1000);
            Press(Enter);
        }

        /// <summary>
        /// Clicks on Approve button.
        /// </summary>
        public void ApproveClick()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria godkend = SearchCriteria.ByText("Accept").AndControlType(ControlType.Button);
            UIItem godkendItem;
            try
            {
                godkendItem = WaitFor(godkend, wnd2);
                Notify("Click on approve button", true);
            }
            catch (Exception e)
            {
                Logger.Log("ApproveClick throws " + e.ToString(), Exc);
                Notify("Cannot click on approve button", false);
                throw;
            }
            godkendItem.Click();
            wnd2.WaitWhileBusy();
            Thread.Sleep(SmallWait);
        }
        /// <summary>
        /// Selects Faktura  (XSL-Uniform) in 'Vclf formular' modal dialog
        /// </summary>
        public void SelectFormular()
        {
            Thread.Sleep(MediumWait);
            Window wnd2 = WaitFor(AppTitle);
            SearchCriteria faktura = SearchCriteria.ByAutomationId(Formular).AndIndex(0);
            UIItem fakturaItem;
            try
            {
                fakturaItem = WaitFor(faktura, wnd2);
                Notify("Select formular from modal dialog", true);
            }
            catch (Exception e)
            {
                Logger.Log("SelectFormular throws " + e.ToString(), Exc);
                Notify("Cannot select formular from modal dialog", false);
                throw;
            }
            fakturaItem.Click();

            Press(Enter);
        }
        /// <summary>
        /// Selects Screen item as a print format and then clicks on OK. Currently method is not used.
        /// </summary>
        public void SelectPrintMethodScreen()
        {
            Thread.Sleep(MediumWait);
            try
            {
                SetText("s");
            }
            catch (Exception e)
            {
                Logger.Log("SelectPrintMethodScreen throws " + e.ToString(), Exc);
                throw;
            }
        }
        /// <summary>
        /// Clicks in some arbitrary place on the screen
        /// </summary>
        public void ClickOnTheScreen(int x, int y)
        {
            Thread.Sleep(MediumWait);
            var point = new Point(x, y);
            Window wnd = WaitFor(AppTitle);
            Desktop.Instance.Mouse.Location = point;
            Desktop.Instance.Mouse.Click();
        }
        /// <summary>
        /// Scrolls to the bottom of document (5 pages are assumed). There is no support for scrolling with the mouse in TestStack White - we have to use keyboard
        /// </summary>
        public void ScrollToTheBottomOfDocument()
        {
            Thread.Sleep(MediumWait);
            for (int i = 0; i < 5; i++)
            {
                Press(PageDown);
                Thread.Sleep(100);
            }
        }
        /// <summary>
        /// Scrolls to the top of document (5 pages are assumed). There is no support for scrolling with the mouse in TestStack White - we have to use keyboard
        /// </summary>
        public void ScrollToTheTopOfDocument()
        {
            Thread.Sleep(MediumWait);
            for (int i = 0; i < 5; i++)
            {
                Press(PageUp);
                Thread.Sleep(100);
            }
        }
        /// <summary>
        /// Scrolls to the right of document. There is no support for scrolling with the mouse in TestStack White - we have to use keyboard
        /// </summary>
        public void ScrollToTheRightOfDocument()
        {
            Thread.Sleep(MediumWait);
            Press(End);
        }
        /// <summary>
        /// Scrolls to the left of document. There is no support for scrolling with the mouse in TestStack White - we have to use keyboard
        /// </summary>
        public void ScrollToTheLeftOfDocument()
        {
            Thread.Sleep(MediumWait);
            Press(Home);
        }
        public void ClosePDFDocument()
        {
            Thread.Sleep(MediumWait);
            List<Window> wnd = Desktop.Instance.Windows();
            try
            {
                foreach (Window win in wnd)
                {
                    if (win.Name.Equals(MainApp))
                    {
                        win.Focus();
                        Thread.Sleep(1000);
                        win.Close();
                        //win.WaitWhileBusy();
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log("ClosePDFDocument throws " + e.ToString(), Exc);
                throw;
            }
            Thread.Sleep(15000);
        }
    }
}
